import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import  PropTypes  from 'prop-types';
import  {Link} from 'react-router-dom'



export default CourseCard;

function CourseCard ({courseProp}) {

    // console.log("Content of Props: ")
    // console.log(props);
    // console.log(typeof props);
    const {_id, name, description, price} = courseProp;
    //State Hooks (useState) - a way to store information within a component and track this information
        // getter, setter
        // variable, function to change the value of a variable
    // const [count, setCount] =useState(0); //count = 0;

    // const [slots, availableSlot] = useState(30);

    // function enroll(){
        
       

        // if(slots > 0){
        //     setCount(count + 1);
        //     console.log('Enrollees: '+ count);
        //     availableSlot(slots - 1);
        //     console.log('slots: ' + slots);
           
             
        // } 
        // else{
        //     alert("No available slots")
        //     document.querySelector('#btn-submit').setAttribute('disabled', true);
        // }

    //     setCount(count + 1);
    //         console.log('Enrollees: '+ count);
    //         availableSlot(slots - 1);
    //         console.log('slots: ' + slots);


    // }

    //useEffect() always run the task on the initial render and/or every render (when the state changes in the component)
    // initial render is when the component is run or displayed for the first time
    // useEffect(() =>{
    //     if (slots === 0){
    //         alert('No slots available');
    //     }
    // }, [slots]);

    return (
        <Card>
    
        <Card.Body>
            <Card.Title>
            {name}
            </Card.Title>

            <Card.Subtitle>
                Description:
            </Card.Subtitle>

            <Card.Text>
            {description}
            </Card.Text>

            <Card.Text>
            Price:
            </Card.Text>

            <Card.Text>
            {price}
            </Card.Text>

            <Button as={Link} to ={`/courses/${_id}`}>Enroll</Button>
          

        </Card.Body>
        </Card>
        
    )
}

// CourseCard.propTypes = {
//     courseProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired,
//     })
// }