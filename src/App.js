// remove: import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
// import { Fragment } from 'react'; /* React Fragments allows us to return multiple elements*/ // s53 to comment
import { BrowserRouter as Router } from 'react-router-dom'; // s53 added
import { Route, Routes } from 'react-router-dom'; //s53 added

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout'; 
import Error from './pages/Error'; 
import CourseView from './pages/CourseView';

// s54 Additional Example
import Settings from './pages/Settings'; 

// s54
import { UserProvider } from './UserContext';

// s55
import { useState, useEffect, useContext } from 'react';

function App() {


  // 1 - create state
  const [user, setUser] = useState({
    id: localStorage.getItem('id'),
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  

  return (
    /* React Fragments allows us to return multiple elements*/

    // 2 - provide/share the state to other components
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/checkEmail" element={<checkEmai />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} /> 
            <Route path="/settings" element={<Settings />} /> 
            <Route path="*" element={<Error />} /> 
          </Routes>
        </Container>
      </ Router>
    </ UserProvider>
    
  );
}

export default App;
