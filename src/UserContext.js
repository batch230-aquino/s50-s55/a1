import React from "react";

// >> Creats a context object
//a context object with object data type can be usede to store information that can be share to other components within the application
const UserContext = React.createContext();
//console.log("content of usercontext: ")
//console.log(userContext)

// The "Provider" component allows other components to consume/use the context object and supply necessary information needed in the context object
// The provider is uesed to create a context that can be consumed.
export const  UserProvider = UserContext.Provider;

export default UserContext;