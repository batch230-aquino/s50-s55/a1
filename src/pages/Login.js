
import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import  UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import '../App.css';
import { FaEye, FaEyeSlash } from 'react-icons/fa'

export default function Login(){

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);

    const [isActive, setIsActive] = useState(true);

    const passwordVisibility = () => setShowPassword(!showPassword);

    
    console.log(email);
    console.log(password);
    

    /*function loginUser(event){
        event.preventDefault();
        localStorage.setItem('email', email);
        //clear input fields

       
        setEmail('');
        setPassword('');
        alert('Log In Succesful!');

        setUser({
            email: localStorage.getItem('email', email)
        })
    }
    */

    function authenticate(event){
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {'Content-Type' : 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data =>{
                console.log(data);
                console.log("Check accessToken: ");
                console.log(data.accessToken);
                retrieveUserDetails(data.accessToken);

                if(typeof data.accessToken !== "undefined"){
                    localStorage.setItem('token', data.accessToken);
                    retrieveUserDetails(data.accessToken);

                    Swal.fire({
                        title:"Login Successful",
                        icon:"success",
                        text:"Welcome to Zuitt!"
                    })
                }
                else{
                    Swal.fire({
                        title:"Login Not Successful",
                        icon:"error",
                        text:"Hindi ka sa Welcome to Zuitt!"
                    })
                }
               
            })
        setEmail('');
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id:data._id,
                isAdmin: data.isAdmin
            })
        })
    }
   

    useEffect(()=>{
        
        if (user.id !== '' && password !== ''){
            
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password])
        return(
           (user.id !== null)? //if true - means email field is not null or has a value
            <Navigate to="/courses" />
            :
                
            <Form onSubmit={(event => authenticate(event))}>
            <h1>Log In</h1>
            <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value = {email}
                            onChange = {event => setEmail(event.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>
    
                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                    <div id= "password">
                        <Form.Control 
                            type={showPassword? 'text' : 'password'} 
                            placeholder="Password" 
                            value = {password}
                            onChange = {event => setPassword(event.target.value)}
                            required
                            
                        />
                        {password ? 
                        <FaEyeSlash onClick={passwordVisibility} id="eye" />  
                        : 
                        <FaEye onClick={passwordVisibility} id="eye"  />
                        }
                    </div>
                    </Form.Group><br/>
                    {isActive ?
                    
                        <Button variant="success" type="submit" id="submitBtn">Submit</Button>
                        :
                        <Button variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
                        }
    
                    
                        
                
            </Form>
    )
}
