import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlight from '../components/Highlight';


const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere",
    destination: "/",
    label: "Enroll now!"
}

export default function Home(){


    return (
        <Fragment>
            <Banner data={data}/>
            <Highlight/>
            
        </Fragment>
    )
}