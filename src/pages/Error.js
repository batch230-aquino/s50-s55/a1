// import Figure from 'react-bootstrap/Figure';
import Banner from '../components/Banner';
/*
function Error() {
    return (
    <div
    style ={{
        alignItems: 'center',
        justifyContent: 'center',
        textAlign:'center'
    }}>
        <Figure>
            <Figure.Image
                width={171}
                height={180}
                alt="171x180"
                src="https://us.123rf.com/450wm/dariachekman/dariachekman2002/dariachekman200200029/141335239-sad-emoji-icon-isolated-on-white-background.jpg"
            />
            <Figure.Caption>
                <h1>Error 404</h1>
                <h5>page not found!</h5>
                <p>please go back to the homepage.</p>
            </Figure.Caption>
        </Figure>
    </div>
        
    );
}

export default Error;
*/

export default function Error(){

    const data ={
    title: "404- Not found",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back home"
    }

    return(
        <Banner data={data}/>
    )
}

