import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from "sweetalert2";

export default function Register(){
    const { user } = useContext(UserContext)
    const navigate = useNavigate();
    //State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNumber, setMobileNumber] =useState('')
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    //State to determine whether submit button is enable or not
    const [isActive, setIsActive] = useState(false);

    //Check if values are successfully binded
    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNumber);
    console.log(password1);
    console.log(password2);


    function registerUser(event){

        event.preventDefault();
        


        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
            method: 'POST',
            headers: {'Content-Type' : 'application/json',
            },
            body: JSON.stringify({email: email})
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data)
            if (data){
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide a different email."
                });
                return;
            }

            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    firstName:firstName,
                    lastName:lastName,
                    email:email,
                    mobileNumber:mobileNumber,
                    password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                // show success message
                console.log(data)
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                });

                navigate ("/login") 
        
        })

    })
    
       
    }



  
    useEffect(()=>{
        
        if ((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNumber !== '') && (password1 === password2)){
            
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [firstName,lastName,email,mobileNumber, password1, password2])

    // const {user, setUser} = useContext(UserContext);
    return(
        //s54 Activity
        (user.id !== null)?
        <navigate to ="/courses"/>
        
        :

        <Form onSubmit={(event => registerUser(event))}>
        <h1>Registration</h1>

        <Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="firstName" 
                        placeholder="First Name" 
                        value = {firstName}
                        onChange = {event => setFirstName(event.target.value)}
                        required
                    />
                    
                </Form.Group>
        <Form.Group controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="lastName" 
                        placeholder="Last Name" 
                        value = {lastName}
                        onChange = {event => setLastName(event.target.value)}
                        required
                    />
                    
                </Form.Group>

        <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                    />
                    
                </Form.Group>

                <Form.Group controlId="mobileNumber">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                        type="mobileNumber" 
                        placeholder="Mobile Number" 
                        value = {mobileNumber}
                        onChange = {event => setMobileNumber(event.target.value)}
                        required
                    />
                    
                </Form.Group>

                

                <Form.Group controlId="password1" className='password1'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value = {password1}
                        onChange = {event => setPassword1(event.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password2" className='password2'>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Verify Password" 
                        value = {password2}
                        onChange = {event => setPassword2(event.target.value)}
                        required
                    />
                </Form.Group><br/>
                {isActive ?
                //true
                    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                    :
                //false
                    <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                    }

                
                    
            
        </Form>
    )
}